/*
 * sortingFunctions.cpp
 *
 *  Created on: 23.05.2017
 *      Author: marce
 */

#include "sortingFunctions.h"
#include <iostream>

void fillArray(int *array, int arraySize) {
	for (int i = 0; i < arraySize; ++i) {
		array[i] = arraySize - i;
	}
}
void printArray(int *array, int arraySize) {
	for (int i = 0; i < arraySize; ++i) {
		std::cout << array[i] << " ";
	}
}
void bubbleSort(int *array, int arraySize) {
	for (int i = 0; i < arraySize - 1; ++i) {
		for (int j = 0; j < arraySize - 1; ++j) {
			if (array[j] > array[j + 1]) {
				array[j] = array[j] - array[j + 1];
				array[j + 1] = array[j + 1] + array[j];
				array[j] = array[j + 1] - array[j];
			}
		}
	}
}
void insertionSort(int *array, int arraySize) {
	for (int i = arraySize - 2; i >= 0; --i) {
		int temp(array[i]);
		int j = i + 1;
		while (temp > array[j] && j < arraySize) {
			array[j - 1] = array[j];
			array[j] = temp;
			++j;
		}
	}
}
void swap(int &a, int &b) { //swaps two integers
	a = a - b;
	b = b + a;
	a = b - a;
}
int partition(int array[], int left, int right) { //divides array by 2 parts,
	int pivot(array[left]);	//in first numbers are less than or equal to pivot
	int i(left);		// in second numbers are greater than or equal to pivot
	int j(right);								// left - lowest array index
	while (true) {								// right - highest array index
		while (array[i] < pivot)
			++i;
		while (array[j] > pivot)
			--j;
		if (i >= j)
			return j;//returns from infinite loop, j is point of array division
		swap(array[i], array[j]);// when conditions in both loops(while) are false array element are swapped
		++i;
		--j;
	}
}
void quickSort(int *array, int left, int right) {
	int partitionPoint;
	if (left < right) {
		partitionPoint = partition(array, left, right); // divides array by 2 smaller parts
		quickSort(array, left, partitionPoint); //recursive call of quicksort for left part of array
		quickSort(array, partitionPoint + 1, right); //recursive call of quicksort for right part of array
	}
}




