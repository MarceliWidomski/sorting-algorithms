/*
 * sortingFunctions.h
 *
 *  Created on: 23.05.2017
 *      Author: marce
 */

#ifndef SORTINGFUNCTIONS_H_
#define SORTINGFUNCTIONS_H_

void fillArray(int *array, int arraySize);
void printArray(int *array, int arraySize);
void bubbleSort(int *array, int arraySize);
void insertionSort(int *array, int arraySize);
void swap(int &a, int &b);
int partition(int *array, int left, int right);
void quickSort(int *array, int left, int right);

#endif /* SORTINGFUNCTIONS_H_ */
