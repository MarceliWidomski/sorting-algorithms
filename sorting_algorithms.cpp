//============================================================================
// Name        : sorting_algorithms.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <ctime>
#include <iomanip>
#include "sortingFunctions.h"
#include "gtest/gtest.h"

int main(int argc, char **argv) {

//	::testing::InitGoogleTest(&argc, argv);
//	return RUN_ALL_TESTS(); // calls all tests

	const int arraySize(10000);

	int *myArray = new int[arraySize];
	fillArray(myArray, arraySize);
	std::cout << "Bubble sort:" << std::endl;
	clock_t start = clock();
	bubbleSort(myArray, arraySize);
	clock_t finish = clock();
	double sortingTime = (1.0 * finish - start) / (CLOCKS_PER_SEC);
	std::cout << "Time of sorting: " << std::setprecision(3) << sortingTime << std::endl;

	int *myArray2 = new int[arraySize];
	fillArray(myArray2, arraySize);
	start = clock();
	insertionSort(myArray2, arraySize);
	finish = clock();
	std::cout << "Insertion sort:" << std::endl;
	sortingTime = (1.0 * finish - start) / (CLOCKS_PER_SEC);
	std::cout << "Time of sorting: " << std::setprecision(3) << sortingTime << std::endl;

	int *myArray3 = new int[arraySize];
	fillArray(myArray3, arraySize);
	start = clock();
	insertionSort(myArray3, arraySize);
	finish = clock();
	std::cout << "Quick sort:" << std::endl;
	sortingTime = (1.0 * finish - start) / (CLOCKS_PER_SEC);
	std::cout << "Time of sorting: " << std::setprecision(3) << sortingTime << std::endl;

	return 0;
}
