/*
 * testing_sorting_algorithms.cpp
 *
 *  Created on: 22.05.2017
 *      Author: RENT
 */

#include "gtest/gtest.h"
#include "sortingFunctions.h"

const int arraySize(5000);

class EmptyArrayTestFixture: public testing::Test {
protected:
	virtual void SetUp() {
		testEmptyArray = new int[0];
	}
	virtual void TearDown() {
		delete[] testEmptyArray;
	}
	int *testEmptyArray;
};
class ArrayTestFixture: public testing::Test {
protected:
	virtual void SetUp() {
		testArray = new int[arraySize];
		fillArray(testArray, arraySize);
	}
	virtual void TearDown() {
		delete[] testArray;
	}
	int *testArray;
};

TEST_F (EmptyArrayTestFixture, EmptyArrayBubbleSortTest) {
	EXPECT_NO_FATAL_FAILURE(bubbleSort(testEmptyArray, 0));
}
TEST_F (EmptyArrayTestFixture, EmptyArrayInsertionSortTest) {
	EXPECT_NO_FATAL_FAILURE(insertionSort(testEmptyArray, 0));
}
TEST_F (EmptyArrayTestFixture, ArrayQuickSortTest) {
	EXPECT_NO_FATAL_FAILURE(quickSort(testEmptyArray, 0, 0));
}
TEST_F (ArrayTestFixture, ArraySetAndFilledCorrectly) {
	EXPECT_EQ(5000, testArray[0]);
	EXPECT_LT(testArray[1], testArray[0]);
	EXPECT_LT(testArray[2500], testArray[2499]);
	EXPECT_LT(testArray[4999], testArray[4998]);
	EXPECT_EQ(1, testArray[4999]);
}
TEST_F (ArrayTestFixture, ArrayBubbleSortTest) {
	bubbleSort(testArray, arraySize);
	EXPECT_EQ(1, testArray[0]);
	EXPECT_GT(testArray[1], testArray[0]);
	EXPECT_GT(testArray[2500], testArray[2499]);
	EXPECT_GT(testArray[4999], testArray[4998]);
	EXPECT_EQ(5000, testArray[4999]);
}
TEST_F (ArrayTestFixture, ArrayInsertionSortTest) {
	insertionSort(testArray, arraySize);
	EXPECT_EQ(1, testArray[0]);
	EXPECT_GT(testArray[1], testArray[0]);
	EXPECT_GT(testArray[2500], testArray[2499]);
	EXPECT_GT(testArray[4999], testArray[4998]);
	EXPECT_EQ(5000, testArray[4999]);
}
TEST_F (ArrayTestFixture, ArrayQuickSortTest) {
	quickSort(testArray, 0, arraySize - 1);
	EXPECT_EQ(1, testArray[0]);
	EXPECT_GT(testArray[1], testArray[0]);
	EXPECT_GT(testArray[2500], testArray[2499]);
	EXPECT_GT(testArray[4999], testArray[4998]);
	EXPECT_EQ(5000, testArray[4999]);
}
TEST_F (ArrayTestFixture, PartOfArrayBubbleSortTest) {
	bubbleSort(testArray+1000, arraySize-2000); // sorts array elements from index 1000 to index 3999
	EXPECT_EQ(5000, testArray[0]);
	EXPECT_EQ(4001, testArray[999]);
	EXPECT_EQ(1000, testArray[4000]);
	EXPECT_EQ(1, testArray[4999]);
	EXPECT_EQ(1001, testArray[1000]);
	EXPECT_EQ(4000, testArray[3999]);
	EXPECT_GT(testArray[1002], testArray[1001]);
	EXPECT_GT(testArray[2500], testArray[2499]);
	EXPECT_GT(testArray[3999], testArray[3998]);
}
TEST_F (ArrayTestFixture, PartOfArrayInsertionSortTest) {
	insertionSort(testArray+1000, arraySize-2000); // sorts array elements from index 1000 to index 3999
	EXPECT_EQ(5000, testArray[0]);
	EXPECT_EQ(4001, testArray[999]);
	EXPECT_EQ(1000, testArray[4000]);
	EXPECT_EQ(1, testArray[4999]);
	EXPECT_EQ(1001, testArray[1000]);
	EXPECT_EQ(4000, testArray[3999]);
	EXPECT_GT(testArray[1002], testArray[1001]);
	EXPECT_GT(testArray[2500], testArray[2499]);
	EXPECT_GT(testArray[3999], testArray[3998]);
}
TEST_F (ArrayTestFixture, PartOfArrayQuicksortSortTest) {
	quickSort(testArray, 1000, 3999); // sorts array elements from index 1000 to index 3999
	EXPECT_EQ(5000, testArray[0]);
	EXPECT_EQ(4001, testArray[999]);
	EXPECT_EQ(1000, testArray[4000]);
	EXPECT_EQ(1, testArray[4999]);
	EXPECT_EQ(1001, testArray[1000]);
	EXPECT_EQ(4000, testArray[3999]);
	EXPECT_GT(testArray[1002], testArray[1001]);
	EXPECT_GT(testArray[2500], testArray[2499]);
	EXPECT_GT(testArray[3999], testArray[3998]);
}
